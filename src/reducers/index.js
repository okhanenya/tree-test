import { combineReducers } from "redux";
import { compose, curry, equals, values, path, filter, any } from "ramda";

import app from "./app";

const id = "app";

export default combineReducers({ app });

const genericGentter = (EntityType, state) =>
  compose(
    values,
    path([id, EntityType.PATH])
  )(state);

export const getDataByType = curry(genericGentter);

export const getFilteredData = (EntityType, ids, state) =>
  compose(
    filter(({ data }) => any(equals(data[EntityType.ID]))(ids)),
    getDataByType(EntityType)
  )(state);
