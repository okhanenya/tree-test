import { path } from "ramda";
import { schema } from "normalizr";
import * as Entities from "../constants/Entities";

// new Entity example: creating new schema
const lol = new schema.Entity(
  Entities.LOLS.PATH,
  {},
  { idAttribute: child => path(["data", Entities.LOLS.ID])(child) }
);

const pohone = new schema.Entity(
  Entities.PHONES.PATH,
  {},
  { idAttribute: child => path(["data", Entities.PHONES.ID])(child) }
);

const relative = new schema.Entity(
  Entities.RELATIVES.PATH,
  {
    data: {},
    kids: {
      has_phone: {
        records: [pohone]
      }
    }
  },
  {
    idAttribute: child => path(["data", Entities.RELATIVES.ID])(child)
  }
);

// new Entity example: adding new schema lol as kid
const parent = new schema.Entity(
  Entities.PARENTS.PATH,
  {
    data: {},
    kids: {
      has_relatives: {
        records: [relative]
      },
      lol_test: {
        records: [lol]
      }
    }
  },
  {
    idAttribute: child => path(["data", Entities.PARENTS.ID])(child)
  }
);

export default [parent];
