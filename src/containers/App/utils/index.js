import React from "react";
import * as R from "ramda";

import Table from "../../../components/Table/Table";
import TableRow from "../../../components/Table/TableRow";

import {
  getKidsIdsByType,
  getEntityTypeByName,
  getKidsTypeNames,
  getHeadersKeys
} from "../../../utils";

export const prepareTable = (
  data,
  getFilteredData,
  ENTITY_TYPE,
  isKid,
  onDelete
) => {
  const columns = getHeadersKeys(data);
  if (R.isEmpty(data)) {
    return null;
  }
  return (
    <Table
      key={ENTITY_TYPE.PATH}
      columns={columns}
      title={isKid ? ENTITY_TYPE.PATH : null}
      nested={isKid}
    >
      {R.values(data).map((rowData, index) =>
        prepareTableRow(
          rowData,
          columns,
          getFilteredData,
          ENTITY_TYPE,
          onDelete,
          (index + 1) % 2 === 0
        )
      )}
    </Table>
  );
};

export const prepareTableRow = (
  rowData,
  columns,
  getFilteredData,
  ENTITY_TYPE,
  onDelete,
  striped
) => {
  const hasKids = !R.isEmpty(rowData.kids);
  const data = columns.map(key => rowData.data[key]);
  const deleteNode = () => onDelete({ entityType: ENTITY_TYPE, rowData });
  return (
    <TableRow
      key={`generic-table-row-${ENTITY_TYPE.PATH}-${
        rowData.data[ENTITY_TYPE.ID]
      }}`}
      rowData={data}
      onDelete={deleteNode}
      striped={striped}
    >
      {hasKids ? prepareKids(rowData.kids, getFilteredData, onDelete) : null}
    </TableRow>
  );
};

export const prepareKids = (kids, getFilteredData, onDelete) =>
  R.filter(kid => !R.isNil(kid))(
    getKidsTypeNames(kids).map((name, index) => {
      const ENTITY_TYPE = getEntityTypeByName(name);
      const ids = getKidsIdsByType(ENTITY_TYPE, kids);
      const data = getFilteredData(ENTITY_TYPE, ids);
      return prepareTable(data, getFilteredData, ENTITY_TYPE, true, onDelete);
    })
  );
