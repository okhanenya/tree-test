import React, { Component } from "react";
import PropTypes from "prop-types";
import { equals } from "ramda";

import { nilOrEmpty } from "../../utils";

const getStyles = (children, striped, isOpen) => ({
  row: {
    height: 32,
    ...(striped && { backgroundColor: "#F0F0F0" })
  },
  arrow: { textAlign: "center", ...(children && { cursor: "pointer" }) },
  delete: { textAlign: "center", cursor: "pointer" },
  kidContainer: { padding: 8 }
});

class TableRow extends Component {
  state = { isOpen: false };

  shouldComponentUpdate(nextProps, nextState) {
    if (
      !equals(this.props.rowData, nextProps.rowData) ||
      !equals(this.props.striped, nextProps.striped) ||
      // children comparing doesn't work correct, require deep comparing
      (nextState.isOpen === true && !equals(this.props.children, nextProps.children)) ||
      !equals(this.state.isOpen, nextState.isOpen)
    ) {
      return true;
    }
    return false;
  }

  handleOpen = () =>
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));

  render() {
    const { rowData, children, onDelete, striped } = this.props;
    const { isOpen } = this.state;
    const styles = getStyles(children, striped, isOpen);
    const arrow = !nilOrEmpty(children) && (isOpen ? "▼" : "▶");
    return [
      <tr key="data" style={styles.row}>
        <td style={styles.arrow} onClick={children ? this.handleOpen : null}>
          {arrow}
        </td>
        {rowData.map((columnValue, index) => (
          <td key={`table-row-column-${index}`}>{columnValue}</td>
        ))}
        <td>
          <span style={styles.delete} onClick={onDelete}>
            ✕
          </span>
        </td>
      </tr>,
      !nilOrEmpty(children) && isOpen && (
        <tr key="kid-data" style={styles.kidRow}>
          <td style={styles.kidContainer} colSpan={rowData.length + 2}>
            {children}
          </td>
        </tr>
      )
    ];
  }
}

TableRow.propTypes = {
  rowData: PropTypes.arrayOf(PropTypes.any).isRequired,
  children: PropTypes.node,
  onDelete: PropTypes.func.isRequired,
  striped: PropTypes.bool
};

TableRow.defaultProps = {
  onDelete: () => {},
  striped: false
};

export default TableRow;
