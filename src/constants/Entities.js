export const PARENTS = {
  PATH: "parents",
  ID: "Identification number"
};

export const RELATIVES = {
  PATH: "has_relatives",
  ID: "Relative ID",
  PARENT: PARENTS.PATH,
  PARENT_FK: "Patient ID"
};

export const PHONES = {
  PATH: "has_phone",
  ID: "Phone ID",
  PARENT: RELATIVES.PATH,
  PARENT_FK: "ID of the relative"
};

// new Entity example: defining new constat for lol schema
export const LOLS = {
  PATH: "lol_test",
  ID: "Lol ID",
  PARENT: PARENTS.PATH,
  PARENT_FK: "Patient ID"
};
