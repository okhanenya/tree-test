import React from "react";
import PropTypes from "prop-types";

const styles = {
  table: {
    width: "100%",
    borderCollapse: "collapse",
    border: "2px solid #DFDFDF",
    padding: 0
  },
  headerRow: { height: 32 },
  header: { textAlign: "left", backgroundColor: "#DADADA" },
  caption: {
    fontWeight: "bold",
    textAlign: "left",
    border: "2px solid #DFDFDF",
    padding: 4
  },
  open: { width: 32 },
  action: { width: 32 }
};

const Table = ({ columns, children, title }) => (
  <table style={styles.table}>
    {title && <caption style={styles.caption}>{title}</caption>}
    <thead style={styles.header}>
      <tr style={styles.headerRow}>
        <th style={styles.open} />
        {columns.map((columnName, index) => (
          <th key={`table-column-${columnName}-${index}`}>{columnName}</th>
        ))}
        <th style={styles.action} />
      </tr>
    </thead>
    <tbody>{children}</tbody>
  </table>
);

Table.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.string).isRequired,
  children: PropTypes.node,
  title: PropTypes.string
};

export default Table;
