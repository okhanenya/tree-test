import {
  path,
  values,
  find,
  propEq,
  keys,
  compose,
  mergeAll,
  map,
  isNil,
  anyPass,
  isEmpty
} from "ramda";

import * as Entities from "../constants/Entities";

export const nilOrEmpty = anyPass([isNil, isEmpty]);

export const getHeadersKeys = compose(
  keys,
  mergeAll,
  map(({ data }) => data)
);

export const getKidsTypeNames = keys;

export const getEntityTypeByName = name => {
  const found = find(propEq("PATH", name))(values(Entities));
  return found ? found : {};
};

export const getKidsIdsByType = (EntityType, kids) =>
  path([EntityType.PATH, "records"])(kids);
