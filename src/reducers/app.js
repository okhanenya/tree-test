import { filter, equals, omit, isEmpty } from "ramda";

import {
  getKidsTypeNames,
  getEntityTypeByName,
  getKidsIdsByType
} from "../utils";
import { getFilteredData } from "../reducers";
import * as ActionTypes from "../constants/ActionTypes";

const initialState = {};

const deleteItem = (entityType, rowData, newState) => {
  if (entityType.PARENT) {
    const _parent = getFilteredData(
      getEntityTypeByName(entityType.PARENT),
      [rowData.data[entityType.PARENT_FK]],
      { app: newState }
    )[0];
    _parent.kids[entityType.PATH].records = filter(
      id => !equals(id, rowData.data[entityType.ID])
    )(_parent.kids[entityType.PATH].records);
    newState[entityType.PARENT][rowData.data[entityType.PARENT_FK]] = _parent;
  }

  newState[entityType.PATH] = omit([rowData.data[entityType.ID]])(
    newState[entityType.PATH]
  );
  return newState;
};

const deleteItemWithKids = (entityType, rowData, newState) => {
  getKidsTypeNames(rowData.kids).forEach(TYPE => {
    const _TYPE = getEntityTypeByName(TYPE);
    const kidsIds = getKidsIdsByType(_TYPE, rowData.kids);
    getFilteredData(_TYPE, kidsIds, { app: newState }).forEach(
      node =>
        (newState = deleteNode({ entityType: _TYPE, rowData: node }, newState))
    );
  });
  newState = deleteItem(entityType, rowData, newState);
  return newState;
};

const deleteNode = ({ entityType, rowData }, state) => {
  let newState = { ...state };
  if (isEmpty(rowData.kids)) {
    newState = deleteItem(entityType, rowData, newState);
  } else {
    newState = deleteItemWithKids(entityType, rowData, newState);
  }
  return newState;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.APP_INITIALIZATION:
      return { ...action.payload.entities };
    case ActionTypes.DELETE_NODE:
      return deleteNode(action.payload, state);
    default:
      return state;
  }
};
