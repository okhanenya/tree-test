import { Component } from "react";
import { connect } from "react-redux";

import { initApp, deleteNode } from "../../actions";
import { getDataByType, getFilteredData } from "../../reducers";
import data from "../../data/data";
import { PARENTS } from "../../constants/Entities";
import { prepareTable } from "./utils";

class App extends Component {
  componentDidMount() {
    this.props.initApp(data);
  }

  render() {
    const { getDataByType, getFilteredData, deleteNode } = this.props;
    const parents = getDataByType(PARENTS);
    return prepareTable(parents, getFilteredData, PARENTS, false, deleteNode);
  }
}

const mapStateToProps = state => ({
  getDataByType: TYPE => getDataByType(TYPE, state),
  getFilteredData: (TYPE, ids) => getFilteredData(TYPE, ids, state)
});
const mapDispatchToProps = { initApp, deleteNode };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
