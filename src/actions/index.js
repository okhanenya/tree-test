import { normalize } from "normalizr";

import appSchema from "./schema";
import * as ActionTypes from "../constants/ActionTypes";

export const initApp = data => ({
  type: ActionTypes.APP_INITIALIZATION,
  payload: normalize(data, appSchema)
});

export const deleteNode = payload => ({
  type: ActionTypes.DELETE_NODE,
  payload
});
