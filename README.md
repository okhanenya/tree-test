# Tree Test

Application with hierarchical table from input data in .json format.

Every item in json consists of it’s own data and array, which items represents child data.  
Item’s data can have a variable number of attributes (key: value) and item can have a variable number of nested child items.  
When you click on item, direct child items are hidden/shown.

The application has data and view layers, which are clearly separated.  
Button "remove", deletes item in data and view layer from application.  
If item has children items, they have to be deleted as well.  

## Demo

https://ataccama-khanenya.herokuapp.com/

## Core

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Stack

- [ramda](https://github.com/ramda/ramda)
- [redux](https://github.com/reduxjs/redux)
- [react-redux](https://github.com/reduxjs/react-redux)
- [normalizr](https://github.com/paularmstrong/normalizr)
- [prop-types](https://github.com/facebook/prop-types)

## Data

Sample data were normalized after app initialization via few schema.Entity what were defined by me in /src/actions/schema.js and use app constants /src/constants/Entities.js

Normalized data are stored in application store what what implemented using redux.

### Adding new Entity type

You can add new kind of Entity, just define new schema in /src/actions/schema.js and constant /src/constants/Entities.js

In source code you can find example by mark "new Entity example", also are prepared modified data available in /src/data/mdata.json  
If you want use this set, just change data location in /src/containers/App/App.js in import section.

## Presentational сomponents

For showing app data, were implemented two components:

- Table.js is a stateless component for showing table values
- TableRow.js is a stateful component for showing row values

available in /src/components/Table

## Containers

This application have one container what connected to redux and use my prepares functions

available in /src/containers/App

## Quck Start

```shell
$ git clone https://okhanenya@bitbucket.org/okhanenya/tree-test.git
$ cd tree-test
$ yarn
$ yarn start
```

### Build application

```shell
$ yarn build
```

## Author

khanenyao © 2018  
[linkedin](https://www.linkedin.com/in/khanenyao/), [github.io](https://khanenyao.github.io)
